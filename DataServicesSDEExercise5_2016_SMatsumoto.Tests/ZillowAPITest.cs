﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DataServicesSDEExercise5_2016_SMatsumoto.Libs.SeachResultAPI;

namespace DataServicesSDEExercise5_2016_SMatsumoto.Tests
{
    [TestClass]
    public class ZillowAPITest
    {
        [TestMethod]
        public void ShouldSucceed()
        {
            // Arrange
            string address = "2114 Bigelow Ave";
            string cityAndState = "Seattle, WA";

            // Act Get from Zillow API
            var results = ZillowAPI.GetSearchResultAsync(address,cityAndState).Result;

            // Assert that response message code = "0"
            Assert.AreEqual("0", results.message.code);
        }

        [TestMethod]
        public void ShouldSucceedWithZipToo()
        {
            // Arrange
            string address = "2114 Bigelow Ave";
            string zip = "98109";

            // Act Get from Zillow API
            var results = ZillowAPI.GetSearchResultAsync(address, zip).Result;

            // Assert that response message code = "0"
            Assert.AreEqual("0", results.message.code);
        }

        [TestMethod]
        public void ShouldFailWithInvalidAddress()
        {
            // Arrange
            string wrongAddress = "1234567 Not Bigelow Ave";
            string zip = "98109";

            // Act Get from Zillow API
            var results = ZillowAPI.GetSearchResultAsync(wrongAddress, zip).Result;

            // Assert that response message code = "507" or "508"
            Assert.IsTrue(results.message.code=="507" || results.message.code == "508");
        }

        [TestMethod]
        public void ShouldFailWithInvalidZWSID()
        {
            // Arrange
            string address = "2114 Bigelow Ave";
            string zip = "98109";
            ZillowAPI.zillowAPIKey = "1234567890123";

            // Act Get from Zillow API
            var results = ZillowAPI.GetSearchResultAsync(address, zip).Result;

            // Assert that response message code = "2"
            Assert.AreEqual("2", results.message.code);
        }

        [TestMethod]
        public void ShouldFailToConnectToZillowAPI()
        {
            // Arrange
            string address = "2114 Bigelow Ave";
            string zip = "98109";
            ZillowAPI.baseUrl = new Uri("https://www.zillow.com/howto/api/WrongGetSearchResults.htm");

            // Act Get from Zillow API
            var results = ZillowAPI.GetSearchResultAsync(address, zip).Result;

            // Assert that response message code starts with INV
            Assert.AreEqual("INV", results.message.code);
        }
        
    }
}
