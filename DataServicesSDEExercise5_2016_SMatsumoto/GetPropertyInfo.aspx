﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Async="true" CodeBehind="GetPropertyInfo.aspx.cs" Inherits="DataServicesSDEExercise5_2016_SMatsumoto.GetPropertyInfo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="Scripts/validation.js"></script>
    <div class="container">
        <div class="form-group">
            <%-- Address field --%>
            <asp:Label runat="server" AssociatedControlID="addressField" Text="Street Address"></asp:Label>
            <asp:TextBox ClientIDMode="Static" runat="server" ID="addressField" AutoCompleteType="HomeStreetAddress" CssClass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ValidationGroup="cityOrZip" Display="Dynamic" runat="server" ControlToValidate="addressField" ErrorMessage="This Field is Required."></asp:RequiredFieldValidator>
            <br />
            <%-- City field --%>
            <asp:Label runat="server" AssociatedControlID="cityField" Text="City"></asp:Label>
            <asp:TextBox ValidationGroup="cityOrZip" ClientIDMode="Static" runat="server" ID="cityField" AutoCompleteType="HomeCity" CssClass="form-control"></asp:TextBox>
            <asp:CustomValidator  Display="Dynamic" runat="server" 
                ClientValidationFunction="validateCityStateOrZip" 
                ErrorMessage="Please enter City and State, or Zip."
                ValidateEmptyText="true"
                ValidationGroup="cityOrZip" >
            </asp:CustomValidator>
            <br />
            <%-- State field --%>
            <asp:Label runat="server" AssociatedControlID="stateField" Text="State"></asp:Label>
            <asp:DropDownList ValidationGroup="cityOrZip" ClientIDMode="Static" runat="server" ID="stateField" AutoCompleteType="HomeState" CssClass="form-control"></asp:DropDownList>
            <asp:CustomValidator  Display="Dynamic" runat="server" 
                ClientValidationFunction="validateCityStateOrZip" 
                ErrorMessage="Please enter City and State, or Zip."
                ValidateEmptyText="true"
                ValidationGroup="cityOrZip" >
            </asp:CustomValidator>
            <br />
            <%-- Zip field --%>
            <asp:Label runat="server" AssociatedControlID="zipField" Text="Zip"></asp:Label>
            <asp:TextBox ValidationGroup="cityOrZip" ClientIDMode="Static" runat="server" ID="zipField" AutoCompleteType="HomeZipCode" CssClass="form-control"></asp:TextBox>
            <asp:CustomValidator Display="Dynamic" runat="server" 
                ClientValidationFunction="validateCityStateOrZip" 
                ErrorMessage="Please enter City and State, or Zip."
                ValidateEmptyText="true"
                ValidationGroup="cityOrZip" >
            </asp:CustomValidator>
            <br />

            <asp:Button runat="server" ID="submitButton" Text="Submit" OnClick="submitButton_Click" ValidationGroup="cityOrZip" />
        </div>
    </div>
    
    <asp:UpdatePanel runat="server" ID="errorMessagePanel" Visible="false">
        <ContentTemplate>
            <table class="table table-bordered  ">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Message</th>
                        <th>Solution</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span runat="server" id="errorCode"></span></td>
                        <td><span runat="server" id="errorMessage"></span></td>
                        <td><span runat="server" id="errorSolution"></span></td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel runat="server" ID="searchResultPanel" Visible="false">
        <ContentTemplate>
            <table id="basic-info" class="search-result table table-bordered">
                <tbody>
                    <tr>
                        <td colspan="2">Zillow Property ID</td>
                        <td><span runat="server" id="zpid"></span></td>
                    </tr>
                    <tr>
                        <td colspan="2">Address</td>
                        <td><span runat="server" id="fullAddress"></span></td>
                    </tr>

                    <tr>
                        <td rowspan="4">Links</td>
                        <td>Home details page</td>
                        <td ><a runat="server" id="homeDetails" ></a></td>
                    </tr>
                    <tr>
                        <td>Chart data page</td>
                        <td ><a runat="server" id="chartData" ></a></td>
                    </tr>
                    <tr>
                        <td>Map this home page</td>
                        <td ><a runat="server" id="map" ></a></td>
                    </tr>
                    <tr>
                        <td>Similar sales page</td>
                        <td ><a runat="server" id="similarSales" ></a></td>
                    </tr>

                    <%-- Zestimate --%>
                    <tr>
                        <td rowspan="6">Zestimate</td>
                        <td></span>Zestimate ($)</td>
                        <td><span runat="server" id="zestimate">N/A</span></td>
                    </tr>
                    <tr>
                        <td>Last updated date</td>
                        <td><span runat="server" id="lastUpdateDate">N/A</span></td>
                    </tr>
                    <tr>
                        <td>30-day change ($)</td>
                        <td><span runat="server" id="thirtyDayChange">N/A</span></td>
                    </tr>
                    <tr>
                        <td>Valuation range (high) ($)</td>
                        <td><span runat="server" id="valuationHigh">N/A</span></td>
                    </tr>
                    <tr>
                        <td>Valuation range (low) ($)</td>
                        <td><span runat="server" id="valuationLow">N/A</span></td>
                    </tr>
                    <tr>
                        <td>Percentile Value</td>
                        <td><span runat="server" id="percentile">N/A</span></td>
                    </tr>

                    <%-- Rent Zestimate --%>
                    <tr runat="server" id="rentZestimateRow1">
                        <td rowspan="5">Rent Zestimate</td>
                        <td>Zestimate ($)</td>
                        <td><span runat="server" id="zestimateRent">N/A</span></td>
                    </tr>
                    <tr runat="server" id="rentZestimateRow2">
                        <td>Last updated date</td>
                        <td><span runat="server" id="lastUpdateDateRent">N/A</span></td>
                    </tr>
                    <tr runat="server" id="rentZestimateRow3">
                        <td>30-day change ($)</td>
                        <td><span runat="server" id="thirtyDayChangeRent">N/A</span></td>
                    </tr>
                    <tr runat="server" id="rentZestimateRow4">
                        <td>Valuation range (high) ($)</td>
                        <td><span runat="server" id="valuationHighRent">N/A</span></td>
                    </tr>
                    <tr runat="server" id="rentZestimateRow5">
                        <td>Valuation range (low) ($)</td>
                        <td><span runat="server" id="valuationLowRent">N/A</span></td>
                    </tr>

                    <asp:Repeater runat="server" ID="regionsRepeater">
                        <ItemTemplate>
                            <tr>
                                <td rowspan="6">Local Real Estate</td>
                                <td>Region Name</td>
                                <td ><%# Eval("name") %></td>
                            </tr>
                            <tr>
                                <td>Zillow Home Value Index</td>
                                <td runat="server" id="zillowHomeValueIndex"><%# Eval("zindexValue")!= null?Eval("zindexValue"):"N/A" %></td>
                            </tr>
                            <tr>
                                <td>Zillow Home Value Index 1-Yr change</td>
                                <td runat="server" id="zillowHomeValueIndex1YrChange"><%# Eval("zindexOneYearChange")!= null ?Eval("zindexOneYearChange"):"N/A" %></td>
                            </tr>
                            <tr>
                                <td>Region overview Page</td>
                                <td runat="server" id="regionOverview"><a href="<%# Eval("links.overview") %>" ><%# Eval("links.overview")!= null?Eval("links.overview"):"N/A" %></a></td>
                            </tr>
                            <tr>
                                <td>Sale by Owner homes page</td>
                                <td runat="server" id="saleByOwnerHomes"><a href="<%# Eval("links.forSaleByOwner") %>"><%# Eval("links.forSaleByOwner")!= null?Eval("links.forSaleByOwner"):"N/A" %></a></td>
                            </tr>
                            <tr>
                                <td>Sale Homes page</td>
                                <td runat="server" id="saleHomes"><a href="<%# Eval("links.forSale") %>" ><%# Eval("links.forSale")!= null?Eval("links.forSale"):"N/A" %></a></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table> 
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>