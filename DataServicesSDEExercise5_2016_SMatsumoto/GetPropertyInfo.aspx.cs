﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;

using DataServicesSDEExercise5_2016_SMatsumoto.Libs.SeachResultAPI;

namespace DataServicesSDEExercise5_2016_SMatsumoto
{
    public partial class GetPropertyInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {

            }
            else
            {
                Title = "Get Property Info!";
                // Set 50 states.
                stateField.Items.Clear();
                stateField.Items.Add("");
                stateField.Items.AddRange(Libs.Utility.AddressHelper.States.AsEnumerable().Select(x => new ListItem(x)).ToArray());
            }
        }

        /// <summary>
        /// Submit button handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void submitButton_Click(object sender, EventArgs e)
        {
            // Hide result on submit.
            errorMessagePanel.Visible = false;
            searchResultPanel.Visible = false;
            HideRentZestimate();

            var address = addressField.Text;
            var citystatezip = zipField.Text.Length>0? zipField.Text : cityField.Text + "," + stateField.Text;
            
            RegisterAsyncTask(new PageAsyncTask(() => GetSearchResultsFromZillowAPI(address: address, cityStateZip:citystatezip)));
        }

        /// <summary>
        /// Assync method that populates API result into the result table.
        /// </summary>
        /// <param name="address">Street address</param>
        /// <param name="cityStateZip">City + State or zip</param>
        /// <returns></returns>
        async Task GetSearchResultsFromZillowAPI(string address, string cityStateZip)
        {
            try
            {
                var results = await ZillowAPI.GetSearchResultAsync(address, cityStateZip);
                
                if (results.message.code != "0")
                {
                    // If not success
                    errorCode.InnerText = SetNullOreEmptyValue(results.message.code);
                    errorMessage.InnerText = SetNullOreEmptyValue(results.message.text);
                    errorSolution.InnerText = GetErrorSolution(results.message.code);
                    if (results.message.code == "1" || results.message.code == "2")
                    {
                        // Administrato must handle this error.
                        NotifyAdministrator("API not working. Code: "+results.message.code);
                    }
                    errorMessagePanel.Visible = true;
                }
                else
                {
                    if(results.response != null && results.response.results.Count() >= 0)
                    {
                        var result = results.response.results.First();
                        zpid.InnerText = SetNullOreEmptyValue(result.zpid.ToString());
                        fullAddress.InnerText = result.address.street + ", " +
                            result.address.city + ", " +
                            result.address.state + ", " +
                            result.address.zipcode + " (lat.: " +
                            result.address.latitude + ", long.: " +
                            result.address.longitude + ")";
                        homeDetails.InnerText = homeDetails.HRef = SetNullOreEmptyValue(result.links.homedetails);
                        chartData.InnerText = chartData.HRef = SetNullOreEmptyValue(result.links.graphsanddata);
                        map.InnerText = map.HRef = SetNullOreEmptyValue(result.links.mapthishome);
                        similarSales.InnerText = similarSales.HRef = SetNullOreEmptyValue(result.links.comparables);

                        if (result.zestimate != null)
                        {
                            zestimate.InnerText = SetNullOreEmptyValue(result.zestimate.amount.Value);
                            lastUpdateDate.InnerText = SetNullOreEmptyValue(result.zestimate.lastupdated);
                            thirtyDayChange.InnerText = SetNullOreEmptyValue(result.zestimate.valueChange.Value);
                            valuationHigh.InnerText = SetNullOreEmptyValue(result.zestimate.valuationRange.high.Value);
                            valuationLow.InnerText = SetNullOreEmptyValue(result.zestimate.valuationRange.low.Value);
                            percentile.InnerText = SetNullOreEmptyValue(result.zestimate.percentile);
                        }
                        if (result.rentzestimate != null)
                        {
                            zestimateRent.InnerText = SetNullOreEmptyValue(result.rentzestimate.amount.Value);
                            lastUpdateDateRent.InnerText = SetNullOreEmptyValue(result.rentzestimate.lastupdated);
                            thirtyDayChangeRent.InnerText = SetNullOreEmptyValue(result.rentzestimate.valueChange.Value);
                            valuationHighRent.InnerText = SetNullOreEmptyValue(result.rentzestimate.valuationRange.high.Value);
                            valuationLowRent.InnerText = SetNullOreEmptyValue(result.rentzestimate.valuationRange.low.Value);
                            DisplayRentZestimate();
                        }

                        regionsRepeater.DataSource = result.localRealEstate;
                        regionsRepeater.DataBind();

                        Title = address;

                        searchResultPanel.Visible = true;
                    }
                    
                }

            }
            catch(Exception ex)
            {
                errorCode.InnerText = "N/A";
                errorMessage.InnerText = "Unexpected Error";
                errorSolution.InnerText = "We are working on getting this fixed, please try again later.";
                errorMessagePanel.Visible = true;

                NotifyAdministrator(ex);
            }
            
        }
        
        /// <summary>
        /// Return "N/A" if input string is null or empty.
        /// </summary>
        /// <param name="val">input string</param>
        /// <returns>"N/A" if input string is null or empty, returns input otherwise.</returns>
        string SetNullOreEmptyValue(string val)
        {
            val = string.IsNullOrEmpty(val) ? "N/A" : val;

            return val;
        }

        /// <summary>
        /// Get Solution to the specified error code.
        /// </summary>
        /// <param name="errorCode">Error code returned from API.</param>
        /// <returns></returns>
        string GetErrorSolution(string errorCode)
        {
            switch (errorCode)
            {
                case "1": return "Error! We are working on getting this fixed, please try again later.";
                case "2": return "Error! We are working on getting this fixed, please try again later.";
                case "3": return "The Zillow Web Service is currently not available. Please come back later and try again.";
                case "4": return "The Zillow Web Service is currently not available. Please come back later and try again.";
                case "500": return "Check if the input address matches the format specified in the input parameters table. When inputting a city name, include the state too. A city name alone will not result in a valid address.";
                case "501": return "Same as error 500.";
                case "502": return "Sorry, the address you provided is not found in Zillow's property database.";
                case "503": return "Please check to see if the city/state you entered is valid. If you provided a ZIP code, check to see if it is valid.";
                case "504": return "The specified area is not covered by the Zillow property database. To see our property coverage tables, click here.";
                case "505": return "Your request timed out. The server could be busy or unavailable. Try again later.";
                case "506": return "If address is valid, try using abbreviations.";
                case "507": return "Verify that the given address is correct.";
                case "508": return "Verify that the given address is correct.";
                default: return "";
            }
        }

        /// <summary>
        ///  Notify error to administrator
        /// </summary>
        /// <param name="ex">Exception</param>
        void NotifyAdministrator(Exception ex)
        {
            // Notify error to administrator
        }

        /// <summary>
        ///  Notify error to administrator
        /// </summary>
        /// <param name="message">Message to send</param>
        void NotifyAdministrator(string message)
        {
            // Notify error to administrator
        }

        /// <summary>
        /// Display Rent Zestimate
        /// </summary>
        void DisplayRentZestimate()
        {
            rentZestimateRow1.Visible = true;
            rentZestimateRow2.Visible = true;
            rentZestimateRow3.Visible = true;
            rentZestimateRow4.Visible = true;
            rentZestimateRow5.Visible = true;
        }

        /// <summary>
        /// Hide Rent Zestimate
        /// </summary>
        void HideRentZestimate()
        {
            rentZestimateRow1.Visible = false;
            rentZestimateRow2.Visible = false;
            rentZestimateRow3.Visible = false;
            rentZestimateRow4.Visible = false;
            rentZestimateRow5.Visible = false;
        }
    }
}