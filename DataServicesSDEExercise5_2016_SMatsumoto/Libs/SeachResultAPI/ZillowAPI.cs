﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Configuration;

using DataServicesSDEExercise5_2016_SMatsumoto.Libs.SeachResultAPI;
//using ZillowNamespace;

namespace DataServicesSDEExercise5_2016_SMatsumoto.Libs.SeachResultAPI
{
    public class ZillowAPI
    {
        // Create onlye one client in the entire applciation to avoid SocketException
        static HttpClient client = new HttpClient();
        public static string zillowAPIKey;
        public static Uri baseUrl;
        
        static ZillowAPI()
        {
            // Get URL and APIKey from Web.config
            string apiUrlFromConfig = ConfigurationManager.AppSettings["GetSearchResultsAPIUrl"];
            if (string.IsNullOrEmpty(apiUrlFromConfig)){
                throw new InvalidOperationException("GetSearchResultsAPIUrl is missing from Web.config.");
            }
            else
            {
                if (!apiUrlFromConfig.EndsWith("?"))
                    apiUrlFromConfig += "?";

                baseUrl = new Uri(apiUrlFromConfig);
            }

            string apiKeyFromConfig = ConfigurationManager.AppSettings["ZillowAPIKey"];
            if (string.IsNullOrEmpty(apiKeyFromConfig)){
                throw new InvalidOperationException("ZillowAPIKey is missing from Web.config.");
            }
            else
            {
                zillowAPIKey = apiKeyFromConfig;
            }
        }

        /// <summary>
        /// Get SearchResult from Zillow API GetSearchResults API
        /// </summary>
        /// <param name="request">Request body contains address (e.g. 2114 Bigelow Ave) and citystatezip (e.g. Seattle+WA    </param>
        /// <returns>
        /// The result of HTTP Get to Zillow API
        /// Check searchresults.message.code for error detail.
        /// 
        /// CODE  DESCRIPTION RESOLUTION
        /// 0	Request successfully processed	
        /// 1	Service error-there was a server-side error while processing the request Check to see if your url is properly formed: delimiters, character cases, etc.
        /// 2	The specified ZWSID parameter was invalid or not specified in the request   Check if you have provided a ZWSID in your API call.If yes, check if the ZWSID is keyed in correctly.If it still doesn't work, contact Zillow to get help on fixing your ZWSID.
        /// 3	Web services are currently unavailable The Zillow Web Service is currently not available.Please come back later and try again.
        /// 4	The API call is currently unavailable   The Zillow Web Service is currently not available.Please come back later and try again.
        /// 500	Invalid or missing address parameter Check if the input address matches the format specified in the input parameters table.When inputting a city name, include the state too. A city name alone will not result in a valid address.
        /// 501	Invalid or missing citystatezip parameter Same as error 500.
        /// 502	No results found Sorry, the address you provided is not found in Zillow's property database.
        /// 503	Failed to resolve city, state or ZIP code   Please check to see if the city/state you entered is valid.If you provided a ZIP code, check to see if it is valid.
        /// 504	No coverage for specified area  The specified area is not covered by the Zillow property database.To see our property coverage tables, click here.
        /// 505	Timeout Your request timed out. The server could be busy or unavailable.Try again later.
        /// 506	Address string too long If address is valid, try using abbreviations.
        /// 507	No exact match found.Verify that the given address is correct.
        /// 508	No exact match found.Verify that the given address is correct.
        /// 
        /// HTTP Http Request failed with HttpStatusCode
        /// REQ Other network issue
        /// INV Iivalid response from Zillow API, failed to deserialize.
        /// </returns>
        public static async Task<searchresults> GetSearchResultAsync(string address, string cityStateZip)
        {
            searchresults result = null;
            
            // Build query
            var query = HttpUtility.ParseQueryString("");
            query["zws-id"] = zillowAPIKey;
            query["address"] = address;
            query["citystatezip"] = cityStateZip;
            query["rentzestimate"] = "true";
            string test = baseUrl.ToString() + query.ToString();

            HttpResponseMessage response;
            try
            {
                response = await client.GetAsync(baseUrl.ToString() + query.ToString());
            }
            catch (HttpRequestException ex)
            {
                result = new searchresults();
                result.message = new Message();
                result.message.code = "REQ";
                result.message.text = ex.Message;
                return result;
            }

            // User XML Serializer
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(searchresults));
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                using (var ms = new System.IO.MemoryStream(System.Text.UTF8Encoding.UTF8.GetBytes(data)))
                {
                    try
                    {
                        result = response.Content.ReadAsAsync<searchresults>().Result;
                        result = (searchresults)serializer.Deserialize(ms);
                    }
                    catch
                    {
                        result = new searchresults();
                        result.message = new Message();
                        result.message.code = "INV";
                        result.message.text = "Invalid response from Zillow API.";
                    }
                }
            }
            else
            {
                result = new searchresults();
                result.message = new Message();
                result.message.code = "HTTP";
                result.message.text = "Connection error to Zillow API Server. Http Status Code: " + response.StatusCode;
            }
            return result;
        }
    }
}