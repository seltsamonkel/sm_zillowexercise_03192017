﻿function validateCityStateOrZip(source, args) {
    if ($("#cityField").val() == "" && $("#stateField").val() == "" && $("#zipField").val() == "")
        args.IsValid = false;
    else {
        if (($("#cityField").val() == "" || $("#stateField").val() == "") && $("#zipField").val() == "")
            args.IsValid = false;
        else
            args.IsValid = true;
    }
}
